# Tools for working with the PowerSpy2 power analyzer

## PowerSpy2

See https://www.alciom.com/en/our-trades/products/powerspy2/ for
protocol documentation.

## Bluetooth Setup

The PowerSpy2 shows up on a regular (non-LE) Bluetooth scan as
`PowerSpy_XXXX` (where `XXXX` match the first two bytes of its EEPROM
content).

Connect/pair with that using PIN "1234". If you are asked to confirm
it shows a given numerical security code just confirm that, the PowerSpy2
itself can't show that anywhere.

These steps are usually only necessary once per host device.

Establish the RFCOMM connection:
```
rfcomm connect /dev/rfcomm0 <btaddr> 1
```

The Bluetooth address of the PowerSpy2 is either shown in your Bluetooh UI,
or can be queried on the command line using `bluetoothctl devices`.

This needs to be done as root, and is not persisted over reboots of either
the PowerSpy2 or the host machine.

TODO: find a way to do this automatically

## Using `powerspy2.py`

### Getting started

The `powerspy2.py` is more or less a 1:1 implementation of the PowerSpy2
serial communication protocol. The probably most interesting command
is the one for realtime measurements:

```
./powerspy2 measure
```

This will output the voltage, current and power values reported by the
PowerSpy2 continuously, until receiving SIGINT/CTRL+C (it's strongly
recommended to terminate it that way, otherwise measuring on the PowerSpy2
is not properly disabled again).

### Commands

TODO document all commands
